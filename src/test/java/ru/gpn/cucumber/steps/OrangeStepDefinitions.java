package ru.gpn.cucumber.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class OrangeStepDefinitions {

    @After
    public void tearDown() {
        WebDriverRunner.closeWebDriver();
    }


    @Given("user is on main page")
    public void user_is_on_main_page() {
        open("https://opensource-demo.orangehrmlive.com");
    }
    @When("user enters username {string}")
    public void user_enters_username(String string) {
        $x("//*[@id='divUsername']/input").sendKeys("Admin");
    }
    @When("user enters pass {string}")
    public void user_enters_pass(String string) {
        $x("//*[@id='divPassword']/input").sendKeys("admin123");
    }
    @When("user clicks on Login button")
    public void user_clicks_on_login_button() {
        $x("//*[@id='btnLogin']").click();
    }
    @When("user click on {string}")
    public void user_click_on(String string) {
        $x("//a[@id='menu_recruitment_viewRecruitmentModule']").click();
    }
    @Then("Candidate section is present")
    public void candidate_section_is_present() {
        // Write code here that turns the phrase above into concrete actions
        $x("//div[@id='srchCandidates']").shouldBe(Condition.visible);
    }
}
