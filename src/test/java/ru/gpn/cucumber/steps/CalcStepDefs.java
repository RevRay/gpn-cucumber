package ru.gpn.cucumber.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import ru.gpn.cucumber.Calc;

public class CalcStepDefs {

    int addResult = 0;

    @When("user adds {int} and {int}")
    public void userAddsAnd(Integer int1, Integer int2) {
        // Write code here that turns the phrase above into concrete actions
        addResult = Calc.add(int1, int2);
    }
    @Then("calculator result is {int}")
    public void calculatorResultIs(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertEquals(addResult, int1, "Add result is invalid!");
    }
}
