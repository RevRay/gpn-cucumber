package ru.gpn.cucumber.steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinitions {
    @Given("user is on start page")
    public void user_is_on_start_page() {
        // Write code here that turns the phrase above into concrete actions
//        throw new io.cucuUser l");
    }
    @When("user enters login vasya")
    public void user_enters_login_vasya() {
        // Write code here that turns the phrase above into concrete actions
//        throw new io.cucumber.java.PendingException();
        System.out.println("User logs in with login = vasya");
    }
    @When("user enters login {string}")
    public void user_enters_login(String string) {
        // Write code here that turns the phrase above into concrete actions
        System.out.printf("User logs in with login = %s\n", string);
    }

    @When("user enters password secure_password")
    public void user_enters_password_secure_password() {
        // Write code here that turns the phrase above into concrete actions
//        throw new io.cucumber.java.PendingException();
        System.out.println("User logs in with password = secure_password");
    }
    @When("user clicks Login button")
    public void user_clicks_login_button() {
        // Write code here that turns the phrase above into concrete actions
//        throw new io.cucumber.java.PendingException();
    }
    @Then("user sees Logout button in top-right corner")
    public void user_sees_logout_button_in_top_right_corner() {
        // Write code here that turns the phrase above into concrete actions
//        throw new io.cucumber.java.PendingException();
    }

    @Then("user sees warning - no pass entered")
    public void userSeesWarningNoPassEntered() {
        // Write code here that turns the phrase above into concrete actions
//        throw new io.cucumber.java.PendingException();
    }

    public void пользовтельЛогинитсяВАдминку() {

    }

    @Before
    public void before() {
        System.out.println("wd started");
    }

    @After
    public void after() {
        System.out.println("wd stopped");

    }

    @Given("we have calculator")
    public void weHaveCalculator() {
        // Write code here that turns the phrase above into concrete actions
        System.out.println("#####");
    }
    @When("we have single calculator")
    public void weHaveSingleCalculator() {
        System.out.println("*****");

    }


}
