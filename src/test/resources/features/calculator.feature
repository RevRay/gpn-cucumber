@id
Feature: Addition

  Background:
    Given we have calculator
    When we have single calculator

  @regress
  Scenario: adding two positive integers
    When user adds 1 and 2
    Then calculator result is 3

  @regress
  Scenario: adding two positive integers
    When user adds 1 and 2
    Then calculator result is 3