@smoke
@DB
@regress
Feature: Authorization feature
# library cucumber - gherkin synthax
  Scenario Outline: Positive authentication with login and password
    Given user is on start page
    When user enters login <login>
    And user enters password secure_password
    And user clicks Login button
    Then user sees Logout button in top-right corner

    Examples:
    |login|password|
    |vasya|324234  |

  Scenario: Positive authentication with login but without password
    Given user is on start page
    When user enters login vasya
    And user clicks Login button
    Then user sees warning - no pass entered

    # BDD - given() when() then()