Feature: Recruitment feature
  Scenario Outline:
    Given user is on main page
    When user enters username "<user>"
    And user enters pass "<pass>"
    And user clicks on Login button
    And user click on "Recruitment section"
    Then Candidate section is present

    Examples:
    |user|pass|
    |Admin|admin123|
    |sdfsdf|1234324|