Feature: Addition
  @regress
    @smoke
    @sanity
  Scenario Outline: Various addition operations
    When user adds <firstArgument> and <secondArgument>
    Then calculator result is <result>

  Examples:
    |firstArgument|secondArgument|result|
    |2            |2             |4     |
    |-5           |-8            |-13   |
    |0            |10            |9     |
